#pragma once
#include "precomp.h"

class PlayerSelection
{
  public:
	PlayerSelection(Canvas gc, ResourceManager resources, int* gameState, Point* mousePos, bool* clicked, int* player);
    ~PlayerSelection();

	void draw();

  private:
	Canvas m_Gc;
	ResourceManager m_Resources;
	int* m_GameState;
	Point* m_MousePos;
	Font m_Font;
	Sprite m_Button;
	Sprite m_ButtonOver;
	bool* m_Clicked;
	int* m_Player;
};