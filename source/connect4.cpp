#include "connect4.h"

float timer = 0.0f;
float fps_timer = 0.0f;
#define FPS_LIMIT 100

Connect4::Connect4(void) : m_Board(NULL), m_Human(NULL), m_AI(NULL), m_Player(NULL), m_HasWon(false), m_Quit(false), m_GameState(0), m_Clicked(false)
{
	//m_Board = new Board(m_Gc, m_Resources);
	//m_Human = new Human(m_Board);
	//m_AI = new AI(m_Board, 4);
	m_Player = 0;
	m_MousePos.x = -1;
	m_MousePos.y = -1;
}

Connect4::~Connect4(void)
{
}

int Connect4::start(const std::vector<string> &args)
{
	//m_Quit = false;

	DisplayWindowDescription desc;
	desc.set_title("Connect 4");
	desc.set_size(Size(500, 450), true);
	desc.set_allow_resize(true);

	DisplayWindow window(desc);
	InputContext ic = window.get_ic();
	InputDevice keyboard = ic.get_keyboard();

	Slot slot_quit = window.sig_window_close().connect(this, &Connect4::on_window_close);
	Slot slot_input_up = (window.get_ic().get_keyboard()).sig_key_up().connect(this, &Connect4::on_input_up);
	Slot mouse_up = (window.get_ic().get_mouse()).sig_key_up().connect(this, &Connect4::on_mouse_up);
	Slot mouse_move = (window.get_ic().get_mouse()).sig_pointer_move().connect(this, &Connect4::on_mouse_move);

	Canvas gc(window);

	int last_time = static_cast<int>(System::get_time());

	ResourceManager resources = XMLResourceManager::create(XMLResourceDocument("resources/resources.xml"));

	//m_Board = new Board(gc, resources, &m_GameState, &m_MousePos, &m_Clicked, &m_Player, &m_HasWon);
	m_PlayerSelection = new PlayerSelection(gc, resources, &m_GameState, &m_MousePos, &m_Clicked, &m_Player);
	m_LevelSelection = new LevelSelection(gc, resources, &m_GameState, &m_MousePos, &m_Clicked, &m_Horizon);
	m_EndScreen = new EndScreen(gc, resources);

	while (!m_Quit)
	{
		int current_time = static_cast<int>(System::get_time());
		float time_delta_ms = static_cast<float>(current_time - last_time);
		timer += time_delta_ms / 1000.0f;
		fps_timer += time_delta_ms / 1000.0f;
		last_time = current_time;

		if(fps_timer > (1.0f / FPS_LIMIT))
		{
			fps_timer = 0.0f;
			//update
		}

		gc.clear(Colorf(0.0f,0.0f,0.0f));

		if (m_GameState == 0)
		{
			m_PlayerSelection->draw();
		}
		if (m_GameState == 1)
		{
			m_LevelSelection->draw();
		}
		if (m_GameState == 2)
		{
			if (!m_AI)
			{
				m_Board = new Board(gc, resources, &m_GameState, &m_MousePos, &m_Clicked, &m_Player, &m_HasWon);
				m_AI = new AI(m_Board, m_Horizon);
			}
			if (m_Player%2 == 1)
			{
				if(m_AI->makeMove())
				{
					m_GameState += 2;
					m_HasWon = true;
				}	
				m_Player++;
			}
			m_Board->draw();
		}
		if (m_GameState == 3)
		{
			m_Board->draw();
			m_EndScreen->draw(true);
		}
		if (m_GameState == 4)
		{
			m_Board->draw();
			m_EndScreen->draw(false);
		}

		window.flip(1);
		KeepAlive::process(0);
	}

	return 0;
}

void Connect4::on_mouse_move(const InputEvent &pointer)
{
	m_Clicked = false;
	m_MousePos.x = pointer.mouse_pos.x;
	m_MousePos.y = pointer.mouse_pos.y;
}

void Connect4::on_mouse_up(const InputEvent &button)
{
	if (button.id == mouse_left)
	{
		m_Clicked = true;
	}
}

void Connect4::on_input_up(const InputEvent &key)
{
	if(key.id == keycode_escape)
	{
		m_Quit = true;
	}
}

void Connect4::on_window_close()
{
	m_Quit = true;
}

