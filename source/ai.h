#pragma once
#include <vector>
#include "board.h"
#include "field.h"

/*struct Field {
		   int row;
		   int column;
};*/

using namespace std;

class AI {
  public:
	  AI(Board* board, int searchDepth);
	  ~AI();

	  int max(Field lastSetPosition, int horizon, int alpha, int beta);
	  int min(Field lastSetPosition, int horizon, int alpha, int beta);
	  bool makeMove();

  private:
	  vector<Field> getPossibleMoves();
	  Board* m_Board;
	  int m_SearchDepth;
	  Field m_BestMove;
	  //int evaluate(int row, int column, char player, int multiply);
	  int evaluate(int multiply);
};