#pragma once
#include "precomp.h"

class LevelSelection
{
  public:
	LevelSelection(Canvas gc, ResourceManager resources, int* gameState, Point* mousePos, bool* clicked, int* horizon);
    ~LevelSelection();

	void draw();

  private:
	Canvas m_Gc;
	ResourceManager m_Resources;
	int* m_GameState;
	Point* m_MousePos;
	Font m_Font;
	Font m_Font2;
	Sprite m_Button;
	Sprite m_ButtonOver;
	bool* m_Clicked;
	int* m_Horizon;
};