#pragma once
#include "endscreen.h"

EndScreen::EndScreen(Canvas gc, ResourceManager resources)
{
	m_Gc = gc;
	m_Resources = resources;
	m_Font = Font::Font(m_Gc,"arial", 32);
}

EndScreen::~EndScreen()
{
}

void EndScreen::draw(bool humanWon)
{
	std::string text;
	int gc_width = m_Gc.get_width();
	int gc_height = m_Gc.get_height();
	if (humanWon)
	{
		text = "Victory!";
	}
	else
	{
		text = "Game Over";
	}

	Size textSize = m_Font.get_text_size(m_Gc, text);

	m_Font.draw_text(m_Gc, gc_width/2 - textSize.width/2, (gc_height/2) - textSize.height, text, Colorf(1.0f, 1.0f, 0.0f, 1.0f));
}