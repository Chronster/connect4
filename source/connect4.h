#pragma once

#include "precomp.h"
#include "board.h"
#include "human.h"
#include "ai.h"
#include "playerselection.h"
#include "levelselection.h"
#include "endscreen.h"

class Connect4
{
public:
	Connect4(void);
	~Connect4(void);
	int start(const std::vector<string> &args);
	int m_GameState;

private:
	Board* m_Board;
	Human* m_Human;
	AI* m_AI;
	int m_Player;
	bool m_HasWon;
	bool m_Quit;
	Point m_MousePos;
	PlayerSelection* m_PlayerSelection;
	LevelSelection* m_LevelSelection;
	EndScreen* m_EndScreen;
	bool m_Clicked;
	int m_Horizon;

	void on_mouse_move(const InputEvent &pointer);
	void on_mouse_up(const InputEvent &button);
	void on_input_up(const InputEvent &key);
	
	void on_window_close();
};

