#include "human.h"

Human::Human(Board* board)
{
	m_Board = board;
}

bool Human::makeMove(int column, int* player) 
{
	vector<Field> moves = getPossibleMoves();
	for(int i=0; i < moves.size(); i++) 
	{
		//if(moves[i].column == atoi(&m_Input)-1)
		if(moves[i].column == column)
		{
			m_Board->data[moves[i].row][moves[i].column] = 'o';
			*player += 1;
			return m_Board->hasWon(moves[i]);
		}
	}
}

vector<Field> Human::getPossibleMoves()
{
	vector<Field> moves;
	for(int w = 0; w < m_Board->getWidth(); w++)
	{
		for(int h = 0; h < m_Board->getHeight(); h++)
		{
			if (m_Board->data[h][w] == 0)
			{
				Field move;
				move.row = h;
				move.column = w;
				moves.push_back(move);
				break;
			}
		}
	}
	return moves;
}