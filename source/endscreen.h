#pragma once
#include "precomp.h"

class EndScreen
{
  public:
	EndScreen(Canvas gc, ResourceManager resources);
    ~EndScreen();

	void draw(bool humanWon);

  private:
	Canvas m_Gc;
	ResourceManager m_Resources;
	Font m_Font;
};