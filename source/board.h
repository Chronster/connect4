#pragma once
#include <vector>
#include <iostream>
#include "field.h"
#include "precomp.h"
#include <map>

using namespace std;

class Board {
  public:
	//static Board &GetInstance(); //Singleton
    int getWidth() { return width; }
    int getHeight() { return height; }         
    int turn;

    vector<vector<char>> data; //the actual field
	Board(Canvas gc, ResourceManager resources, int* gameState, Point* mousePos, bool* clicked, int* player, bool* hasWon);
    ~Board();

	void printConsole();
	void draw();
	bool hasWon(Field lastStone);

  private:
	static Board *instance;
	int width;
	int height;
	Canvas m_Gc;
	ResourceManager m_Resources;
	map<char, Sprite> m_BoardTiles;
	int* m_GameState;
	Point* m_MousePos;
	bool* m_Clicked;
	Sprite m_Arrow;
	Sprite m_ArrowOver;
	int* m_Player;
	bool* m_HasWon;

	bool Board::makeMove(int column);
	vector<Field> Board::getPossibleMoves();
};