#pragma once
#include "Board.h";
#include <conio.h>;
#include <vector>;
//#include "ai.h";
#include "field.h";

using namespace std;

class Human
{
  public:
	  Human(Board* board);
	  bool makeMove(int column, int* player);

  private:
	  char m_Input;
	  Board* m_Board;
	  vector<Field> getPossibleMoves();
};