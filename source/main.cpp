#include "precomp.h"
#include "connect4.h"

//using namespace clan;

// Choose the target renderer
//#define USE_SOFTWARE_RENDERER
//#define USE_OPENGL_1
#define USE_OPENGL_2

#ifdef USE_SOFTWARE_RENDERER
#include <ClanLib/swrender.h>
#endif

#ifdef USE_OPENGL_1
#include <ClanLib/gl1.h>
#endif

#ifdef USE_OPENGL_2
#include <ClanLib/gl.h>
#endif

int main(const std::vector<string> &args)
{
	try
	{
		// Initialize ClanLib base components
		SetupCore setup_core;

		// Initialize the ClanLib display component
		SetupDisplay setup_display;

		#ifdef USE_SOFTWARE_RENDERER
			SetupSWRender setup_swrender;
		#endif

		#ifdef USE_OPENGL_1
			SetupGL1 setup_gl1;
		#endif

		#ifdef USE_OPENGL_2
			SetupGL setup_gl;
		#endif

		// Start the Application
		Connect4 app;
		int retval = app.start(args);
		return retval;
	}
	catch(Exception &exception)
	{
		// Create a console window for text-output if not available
		/*ConsoleWindow console("Console", 80, 160);
		Console::write_line("Exception caught: " + exception.get_message_and_stack_trace());
		console.display_close_message();*/

		return -1;
	}
}

// Instantiate CL_ClanApplication, informing it where the Program is located
Application app(&main);