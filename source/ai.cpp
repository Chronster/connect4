#pragma once
#include "ai.h"

AI::AI(Board* board, int searchDepth) 
{
	this->m_Board = board;
	m_SearchDepth = searchDepth;
}

AI::~AI()
{
}

int AI::max(Field lastSetPosition, int horizon, int alpha, int beta)
{
	vector<Field> possibleMoves = getPossibleMoves();

	if (horizon == 0 || possibleMoves.size() == 0)
	{
		//return evaluate(lastSetPosition.row, lastSetPosition.column, 'x', 1);
		return evaluate(1);
	}
    int maxValue = alpha;
	int value;
	Field lastMove; 

	for(int i=0; i<possibleMoves.size(); i++) {
		m_Board->data[possibleMoves[i].row][possibleMoves[i].column] = 'x'; // fuehreNaechstenZugAus;
		
		lastMove.row = possibleMoves[i].row;
		lastMove.column = possibleMoves[i].column;
        
		value = min(lastMove, horizon-1, maxValue, beta);

        m_Board->data[possibleMoves[i].row][possibleMoves[i].column] = 0; // macheZugRueckgaengig;
        if (value > maxValue)
		{
			maxValue = value;
			if (maxValue >= beta)
			{
				break;
			}
			if (horizon == m_SearchDepth)
			{
				m_BestMove = possibleMoves[i];
			}
       }
    }
    return maxValue;
}


int AI::min(Field lastSetPosition, int horizon, int alpha, int beta)
{
	vector<Field> possibleMoves = getPossibleMoves();

	if (horizon == 0 || possibleMoves.size() == 0)
	{
		//return evaluate(lastSetPosition.row, lastSetPosition.column, 'o', 1);
		return evaluate(1);
	}
    int minValue = beta;
	int value;
	Field lastMove; 

	for(int i=0; i<possibleMoves.size(); i++) {
		m_Board->data[possibleMoves[i].row][possibleMoves[i].column] = 'o'; // fuehreNaechstenZugAus;

		lastMove.row = possibleMoves[i].row;
		lastMove.column = possibleMoves[i].column;

        value = max(lastMove, horizon-1, alpha, minValue);

        m_Board->data[possibleMoves[i].row][possibleMoves[i].column] = 0; // macheZugRueckgaengig;
        if (value < minValue)
		{
			minValue = value;
			if (minValue <= alpha)
			{
				break;
			}
       }
    }
    return minValue;
}

vector<Field> AI::getPossibleMoves()
{
	vector<Field> moves;
	for(int w = 0; w < m_Board->getWidth(); w++)
	{
		for(int h = 0; h < m_Board->getHeight(); h++)
		{
			if (m_Board->data[h][w] == 0)
			{
				Field move;
				move.row = h;
				move.column = w;
				moves.push_back(move);
				break;
			}
		}
	}
	return moves;
}

/*int AI::evaluate(int row, int column, char player, int multiply)
{        
        int connected = 0;
        int totalPoints = 0;

        //in every possible direction (horizontal, vertical, diagonal1, diagonal2, we only need to check 3 neighbours to determine if player has won

#pragma region CheckHorizontally
        {

                //If we find 3 neighbours, we have a sum of 4, which is the winning condition
                //check max. 3 values to the left of the current stone

                for (int i = -1; i >= -3; i--)
                {
                        //check if we don't run out of bounds first
                        if (column+i < 0)
                                break; //continue with checking to the right side of the stone instead

						if (m_Board->data[row][column+i] == player)
                                ++connected;
                        else
                                break; //continue checking to the right
                }


                //now check only necessary remaining values to the right of the current stone
                for (int i = 1; i <= 3; i++)
                {
                        //check if we don't run out of bounds first
                        if (column+i > m_Board->getWidth()-1)
                                break; //no more stones to the right exist

                        if (m_Board->data[row][column+i] == player)
                                ++connected;
                        else
                                break; //no more stones to the right are connected
                }
                if (connected == 3)
                        totalPoints += 20; //necessaryPointsForWinning;
                else
                        totalPoints += connected;
        }
#pragma endregion

#pragma region Vertically
        {
                //If we find 3 neighbours, we have a sum of 4, which is the winning condition
                //check max. 3 values to the bottom of the current stone
                //Note: we don't need to check the upper direction, as the current stone is already on top
                connected = 0;
                for (int i = 1; i <= 3; i++)
                {
                        //check if we don't run out of bounds first
                        if (row+i > m_Board->getHeight()-1)
                                break; //no more stones to the right exist

                        if (m_Board->data[row+i][column] == player)
                                ++connected;
                        else
                                break; //no more stones in bottom direction are connected
                }

                //now we can determine if the player got 3 connected stones horizontally (4 in total)
                if (connected == 3)
                        totalPoints += 20; //necessaryPointsForWinning
                else
                        totalPoints += connected;
        }
#pragma endregion


#pragma region CheckDiagonalBottomLeftToTopRight
        {
                //If we find 3 neighbours, we have a sum of 4, which is the winning condition
                //check max. 3 values to the bottom-Left direction of the current stone

                connected = 0;

                for (int i = -1; i >= -3; i--)
                {
                        //check if we don't run out of bounds first
                        if (column+i < 0 || row-i > m_Board->getHeight()-1)
                                break; //continue with checking to the top right direction of the stone instead

                        if (m_Board->data[row-i][column+i] == player)
                                ++connected;
                        else
                                break; //continue checking to the bottom left side of the stone instead
                }


                //now check only necessary remaining values to the top right direction of the current stone
                for (int i = 1; i <= 3; i++)
                {
                        //check if we don't run out of bounds first
                        if (column+i > m_Board->getWidth()-1 || row-i < 0)
                                break; //no more stones in the direction exist

                        if (m_Board->data[row-i][column+i] == player)
                                ++connected;
                        else
                                break; //no more stones in this direction are connected
                }

                if (connected == 3)
                        totalPoints += 20; //necessaryPointsForWinning
                else
                        totalPoints += connected;
        }
#pragma endregion

#pragma region CheckDiagonalBottomRightToTopLeft
        {
                //If we find 3 neighbours, we have a sum of 4, which is the winning condition
                //check max. 3 values to the bottom-right direction of the current stone
                connected = 0;
                for (int i = -1; i >= -3; i--)
                {
                        //check if we don't run out of bounds first
                        if (column-i > m_Board->getWidth()-1 || row-i > m_Board->getHeight()-1)
                                break; //continue with checking to the top left direction of the stone instead

                        if (m_Board->data[row-i][column-i] == player)
                                ++connected;
                        else
                                break; //continue checking to the top left side of the stone instead
                }

                //now check only necessary remaining values to the top Left direction of the current stone
                for (int i = 1; i <= 3; i++)
                {
                        //check if we don't run out of bounds first
                        if (column-i < 0 || row-i < 0)
                                break; //no more stones in the direction exist

                        if (m_Board->data[row-i][column-i] == player)
                                ++connected;
                        else
                                break; //no more stones in this direction are connected
                }

                if (connected == 3)
                        totalPoints += 20; //necessaryPointsForWinning
                else
                        totalPoints += connected;
        }

        if (totalPoints >= 20) //necessaryPointsForWinning
                totalPoints *= multiply; //kick boost for early winning

#pragma endregion

        //give stones in the middle a bonus of 2 points if their in the first row
        if (row == m_Board->getHeight()-1)
        {
                if (column == 3)
                        totalPoints +=2;
                if (column == 2 || column == 4)
                        totalPoints +=1;
        }
        return totalPoints;
}*/

int AI::evaluate(int multiply)
{
	int doubleCountAI = 0;
	int tripleCountAI = 0;
	int quadrupleCountAI = 0;
	int doubleCountHuman = 0;
	int tripleCountHuman = 0;
	int quadrupleCountHuman = 0;

	//horizontal
	for (int row = 0; row < m_Board->getHeight(); row++) 
	{
		for (int column = 0; column < 4; column++) 
		{
			int stoneCountHuman = 0;
			int stoneCountAI = 0;
			
			for (int i = column; i < column+4; i++) 
			{
				if (m_Board->data[row][i] == 'x')
				{
					++stoneCountAI;
				}
				
				else if (m_Board->data[row][i] == 'o')
				{
					++stoneCountHuman;
				}
			}

			if (stoneCountHuman!=0 && stoneCountAI!=0)
			{
				continue;
			}
			
			else
			{
				if (stoneCountAI > 1)
				{
					if (stoneCountAI == 2)
					{
						++doubleCountAI;
					}
				
					else if (stoneCountAI == 3)
					{
						++tripleCountAI;
					}

					else if (stoneCountAI == 4)
					{
						++quadrupleCountAI;
					}
				}

				else
				{
					if (stoneCountHuman == 2)
					{
						++doubleCountHuman;
					}
				
					else if (stoneCountHuman == 3)
					{
						++tripleCountHuman;
					}

					else if (stoneCountHuman == 4)
					{
						++quadrupleCountHuman;
					}
				}
			}
		}
	}

	//vertical
	for (int column = 0; column < m_Board->getWidth(); column++) 
	{
		for (int row = 0; row < 3; row++) 
		{
			int stoneCountHuman = 0;
			int stoneCountAI = 0;
			
			for (int i = row; i < row+4; i++) 
			{
				if (m_Board->data[i][column] == 'x')
				{
					++stoneCountAI;
				}
				
				else if (m_Board->data[i][column] == 'o')
				{
					++stoneCountHuman;
				}
			}

			if (stoneCountHuman!=0 && stoneCountAI!=0)
			{
				continue;
			}
			
			else
			{
				if (stoneCountAI > 1)
				{
					if (stoneCountAI == 2)
					{
						++doubleCountAI;
					}
				
					else if (stoneCountAI == 3)
					{
						++tripleCountAI;
					}

					else if (stoneCountAI == 4)
					{
						++quadrupleCountAI;
					}
				}

				else
				{
					if (stoneCountHuman == 2)
					{
						++doubleCountHuman;
					}
				
					else if (stoneCountHuman == 3)
					{
						++tripleCountHuman;
					}

					else if (stoneCountHuman == 4)
					{
						++quadrupleCountHuman;
					}
				}
			}
		}
	}

	//diagonal1
	vector<Field> diag1Positions;
	Field field;

	
	field.row = 0; field.column = 0; diag1Positions.push_back(field);
	field.row = 1; field.column = 0; diag1Positions.push_back(field);
	field.row = 0; field.column = 1; diag1Positions.push_back(field);
	field.row = 2; field.column = 0; diag1Positions.push_back(field);
	field.row = 1; field.column = 1; diag1Positions.push_back(field);
	field.row = 0; field.column = 2; diag1Positions.push_back(field);
	field.row = 2; field.column = 1; diag1Positions.push_back(field);
	field.row = 1; field.column = 2; diag1Positions.push_back(field);
	field.row = 0; field.column = 3; diag1Positions.push_back(field);
	field.row = 2; field.column = 2; diag1Positions.push_back(field);
	field.row = 1; field.column = 3; diag1Positions.push_back(field);
	field.row = 2; field.column = 3; diag1Positions.push_back(field);

	for (int x = 0; x < diag1Positions.size(); x++) 
	{
		int stoneCountHuman = 0;
		int stoneCountAI = 0;

		int row = diag1Positions[x].row;
		int column = diag1Positions[x].column;
			
		//for (int row = diag1Positions[x].row, int column = diag1Positions[x].column; row < (diag1Positions[x].row)+4; row++, column++) 
		for (; row < diag1Positions[x].row+4; row++, column++)
		{
			if (m_Board->data[row][column] == 'x')
			{
				++stoneCountAI;
			}
				
			else if (m_Board->data[row][column] == 'o')
			{
				++stoneCountHuman;
			}
		}

		if (stoneCountHuman!=0 && stoneCountAI!=0)
		{
			continue;
		}
			
		else
		{
			if (stoneCountAI > 1)
			{
				if (stoneCountAI == 2)
				{
					++doubleCountAI;
				}
				
				else if (stoneCountAI == 3)
				{
					++tripleCountAI;
				}

				else if (stoneCountAI == 4)
				{
					++quadrupleCountAI;
				}
			}

			else
			{
				if (stoneCountHuman == 2)
				{
					++doubleCountHuman;
				}
				
				else if (stoneCountHuman == 3)
				{
					++tripleCountHuman;
				}

				else if (stoneCountHuman == 4)
				{
					++quadrupleCountHuman;
				}
			}
		}
	}

	//diagonal2
	vector<Field> diag2Positions;
	
	field.row = 0; field.column = 6; diag2Positions.push_back(field);
	field.row = 1; field.column = 6; diag2Positions.push_back(field);
	field.row = 0; field.column = 5; diag2Positions.push_back(field);
	field.row = 2; field.column = 6; diag2Positions.push_back(field);
	field.row = 1; field.column = 5; diag2Positions.push_back(field);
	field.row = 0; field.column = 4; diag2Positions.push_back(field);
	field.row = 2; field.column = 5; diag2Positions.push_back(field);
	field.row = 1; field.column = 4; diag2Positions.push_back(field);
	field.row = 0; field.column = 3; diag2Positions.push_back(field);
	field.row = 2; field.column = 4; diag2Positions.push_back(field);
	field.row = 1; field.column = 3; diag2Positions.push_back(field);
	field.row = 2; field.column = 3; diag2Positions.push_back(field);

	for (int x = 0; x < diag2Positions.size(); x++) 
	{
		int stoneCountHuman = 0;
		int stoneCountAI = 0;
		int row = diag2Positions[x].row;
		int column = diag2Positions[x].column;

		//for (int row = diag2Positions[x].row, int column = diag2Positions[x].column; row < diag2Positions[x].row+4; row++, column--) 
		for (; row < diag2Positions[x].row+4; row++, column--)
		{
			if (m_Board->data[row][column] == 'x')
			{
				++stoneCountAI;
			}
				
			else if (m_Board->data[row][column] == 'o')
			{
				++stoneCountHuman;
			}
		}

		if (stoneCountHuman!=0 && stoneCountAI!=0)
		{
			continue;
		}
			
		else
		{
			if (stoneCountAI > 1)
			{
				if (stoneCountAI == 2)
				{
					++doubleCountAI;
				}
				
				else if (stoneCountAI == 3)
				{
					++tripleCountAI;
				}

				else if (stoneCountAI == 4)
				{
					++quadrupleCountAI;
				}
			}

			else
			{
				if (stoneCountHuman == 2)
				{
					++doubleCountHuman;
				}
				
				else if (stoneCountHuman == 3)
				{
					++tripleCountHuman;
				}

				else if (stoneCountHuman == 4)
				{
					++quadrupleCountHuman;
				}
			}
		}
	}
	return 500+10*doubleCountAI+40*tripleCountAI+50000*quadrupleCountAI-10*doubleCountHuman-40*tripleCountHuman-5000000*quadrupleCountHuman;
}

bool AI::makeMove() 
{
	Field dummyPos;
	dummyPos.row = -1;
	dummyPos.column = -1;
	max(dummyPos, m_SearchDepth, -999999999, 999999999);
	m_Board->data[m_BestMove.row][m_BestMove.column] = 'x';
	return m_Board->hasWon(m_BestMove);
}