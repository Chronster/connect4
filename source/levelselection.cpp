#pragma once
#include "levelselection.h"

LevelSelection::LevelSelection(Canvas gc, ResourceManager resources, int* gameState, Point* mousePos, bool* clicked, int* horizon)
{
	m_Gc = gc;
	m_Resources = resources;
	m_GameState = gameState;
	m_MousePos = mousePos;
	m_Font2 = Font::Font(m_Gc,"arial", 32);
	m_Font = Font::Font(m_Gc,"arial", 20);
	m_Button = Sprite::resource(m_Gc, "button", m_Resources);
	m_ButtonOver = Sprite::resource(m_Gc, "buttonover", m_Resources);
	m_Clicked = clicked;
	m_Horizon = horizon;
}

LevelSelection::~LevelSelection()
{
}

void LevelSelection::draw()
{
	int gc_width = m_Gc.get_width();
	int gc_height = m_Gc.get_height();
	float scaleX = 5.0f;
	float scaleY = 0.2f;
	int offset = gc_height/4;
	std::string text = "How brave are you?";
	std::string button1Str = "Piece of Cake";
	std::string button2Str = "Easy";
	std::string button3Str = "Normal";
	std::string button4Str = "Hard";
	std::string button5Str = "Insane";
	std::string button6Str = "Impossible";

    Size font1Size = m_Font.get_text_size(m_Gc, button1Str);
	Size font2Size = m_Font.get_text_size(m_Gc, button2Str);
	Size font3Size = m_Font.get_text_size(m_Gc, button3Str);
	Size font4Size = m_Font.get_text_size(m_Gc, button4Str);
	Size font5Size = m_Font.get_text_size(m_Gc, button5Str);
	Size font6Size = m_Font.get_text_size(m_Gc, button6Str);
	Size textSize = m_Font2.get_text_size(m_Gc, text);

	m_Font2.draw_text(m_Gc, gc_width/2 - textSize.width/2, (gc_height/4) - textSize.height, text, Colorf(1.0f, 1.0f, 0.0f, 1.0f));

	Sprite* button = &m_Button;
	offset += (font1Size.height)*2;
	if (m_MousePos->x >= gc_width/2 - ((button->get_width()*scaleX)/2) && m_MousePos->x <= gc_width/2 + ((button->get_width()*scaleX)/2))
	{
		if (m_MousePos->y >= offset - ((button->get_height()*scaleY)/2) && m_MousePos->y <= offset + ((button->get_height()*scaleY)/2))
		{
			button = &m_ButtonOver;
			if (*m_Clicked)
			{
				*m_GameState += 1;
				*m_Clicked = false;
				*m_Horizon = 1;
			}
		}
	}

	button->set_scale(scaleX, scaleY);
	button->update(10);
	button->draw(m_Gc, (gc_width/2 - ((button->get_width()*scaleX)/2)), offset - ((button->get_height()*scaleY)/2));
	m_Font.draw_text(m_Gc, (gc_width/2 - font1Size.width/2), (offset + font1Size.height/3), button1Str, Colorf(0.0f, 0.0f, 0.0f, 1.0f));

	button = &m_Button;
	offset += (font2Size.height)*2;
	if (m_MousePos->x >= gc_width/2 - ((button->get_width()*scaleX)/2) && m_MousePos->x <= gc_width/2 + ((button->get_width()*scaleX)/2))
	{
		if (m_MousePos->y >= offset - ((button->get_height()*scaleY)/2) && m_MousePos->y <= offset + ((button->get_height()*scaleY)/2))
		{
			button = &m_ButtonOver;
			if (*m_Clicked)
			{
				*m_GameState += 1;
				*m_Clicked = false;
				*m_Horizon = 2;
			}
		}
	}

	button->set_scale(scaleX, scaleY);
	button->update(10);
	button->draw(m_Gc, (gc_width/2 - ((button->get_width()*scaleX)/2)), offset - ((button->get_height()*scaleY)/2));
	m_Font.draw_text(m_Gc, (gc_width/2 - font2Size.width/2), (offset + font2Size.height/3), button2Str, Colorf(0.0f, 0.0f, 0.0f, 1.0f));

	button = &m_Button;
	offset += (font3Size.height)*2;
	if (m_MousePos->x >= gc_width/2 - ((button->get_width()*scaleX)/2) && m_MousePos->x <= gc_width/2 + ((button->get_width()*scaleX)/2))
	{
		if (m_MousePos->y >= offset - ((button->get_height()*scaleY)/2) && m_MousePos->y <= offset + ((button->get_height()*scaleY)/2))
		{
			button = &m_ButtonOver;
			if (*m_Clicked)
			{
				*m_GameState += 1;
				*m_Clicked = false;
				*m_Horizon = 3;
			}
		}
	}

	button->set_scale(scaleX, scaleY);
	button->update(10);
	button->draw(m_Gc, (gc_width/2 - ((button->get_width()*scaleX)/2)), offset - ((button->get_height()*scaleY)/2));
	m_Font.draw_text(m_Gc, (gc_width/2 - font3Size.width/2), (offset + font3Size.height/3), button3Str, Colorf(0.0f, 0.0f, 0.0f, 1.0f));

	button = &m_Button;
	offset += (font4Size.height)*2;
	if (m_MousePos->x >= gc_width/2 - ((button->get_width()*scaleX)/2) && m_MousePos->x <= gc_width/2 + ((button->get_width()*scaleX)/2))
	{
		if (m_MousePos->y >= offset - ((button->get_height()*scaleY)/2) && m_MousePos->y <= offset + ((button->get_height()*scaleY)/2))
		{
			button = &m_ButtonOver;
			if (*m_Clicked)
			{
				*m_GameState += 1;
				*m_Clicked = false;
				*m_Horizon = 4;
			}
		}
	}

	button->set_scale(scaleX, scaleY);
	button->update(10);
	button->draw(m_Gc, (gc_width/2 - ((button->get_width()*scaleX)/2)), offset - ((button->get_height()*scaleY)/2));
	m_Font.draw_text(m_Gc, (gc_width/2 - font4Size.width/2), (offset + font4Size.height/3), button4Str, Colorf(0.0f, 0.0f, 0.0f, 1.0f));

	button = &m_Button;
	offset += (font5Size.height)*2;
	if (m_MousePos->x >= gc_width/2 - ((button->get_width()*scaleX)/2) && m_MousePos->x <= gc_width/2 + ((button->get_width()*scaleX)/2))
	{
		if (m_MousePos->y >= offset - ((button->get_height()*scaleY)/2) && m_MousePos->y <= offset + ((button->get_height()*scaleY)/2))
		{
			button = &m_ButtonOver;
			if (*m_Clicked)
			{
				*m_GameState += 1;
				*m_Clicked = false;
				*m_Horizon = 6;
			}
		}
	}

	button->set_scale(scaleX, scaleY);
	button->update(10);
	button->draw(m_Gc, (gc_width/2 - ((button->get_width()*scaleX)/2)), offset - ((button->get_height()*scaleY)/2));
	m_Font.draw_text(m_Gc, (gc_width/2 - font5Size.width/2), (offset + font5Size.height/3), button5Str, Colorf(0.0f, 0.0f, 0.0f, 1.0f));

	button = &m_Button;
	offset += (font6Size.height)*2;
	if (m_MousePos->x >= gc_width/2 - ((button->get_width()*scaleX)/2) && m_MousePos->x <= gc_width/2 + ((button->get_width()*scaleX)/2))
	{
		if (m_MousePos->y >= offset - ((button->get_height()*scaleY)/2) && m_MousePos->y <= offset + ((button->get_height()*scaleY)/2))
		{
			button = &m_ButtonOver;
			if (*m_Clicked)
			{
				*m_GameState += 1;
				*m_Clicked = false;
				*m_Horizon = 9;
			}
		}
	}

	button->set_scale(scaleX, scaleY);
	button->update(10);
	button->draw(m_Gc, (gc_width/2 - ((button->get_width()*scaleX)/2)), offset - ((button->get_height()*scaleY)/2));
	m_Font.draw_text(m_Gc, (gc_width/2 - font6Size.width/2), (offset + font6Size.height/3), button6Str, Colorf(0.0f, 0.0f, 0.0f, 1.0f));
}