#pragma once
#include "Board.h"

Board *Board::instance = 0;

//Map Initialization
Board::Board(Canvas gc, ResourceManager resources, int* gameState, Point* mousePos, bool* clicked, int* player, bool* hasWon) : width(7), height(6), turn(0)
{         
		m_Gc = gc;
		m_Resources = resources;
		m_Player = player;
		m_MousePos = mousePos;
		m_Clicked = clicked;
		m_HasWon = hasWon;
		m_GameState = gameState;
        this->data = vector<vector<char>>(height,vector<char>(width));

        for(int row = 0; row < height; ++row)
        {
                for(int column = 0; column < width; ++column)
                {
                        this->data[row][column] = 0;
        
                }
        }

		//Load sprites
		m_BoardTiles.insert(make_pair('0', Sprite::resource(m_Gc, "boardtile", m_Resources)));
		if (*m_Player == 0) 
		{
			m_BoardTiles.insert(make_pair('o', Sprite::resource(m_Gc, "stoneyellow", m_Resources)));
			m_BoardTiles.insert(make_pair('x', Sprite::resource(m_Gc, "stonered", m_Resources)));
		}

		else
		{
			m_BoardTiles.insert(make_pair('x', Sprite::resource(m_Gc, "stoneyellow", m_Resources)));
			m_BoardTiles.insert(make_pair('o', Sprite::resource(m_Gc, "stonered", m_Resources)));
		}

		m_Arrow = Sprite::resource(m_Gc, "arrow", m_Resources);
		m_ArrowOver = Sprite::resource(m_Gc, "arrowover", m_Resources);
}

/*Board &Board::GetInstance() //Singleton
{
        if (!instance)
                instance =  new Board();
        return *instance;
}*/

void Board::printConsole()
{
	cout << endl;
	for(int row = this->getHeight()-1; row >= 0; --row)
    {
			for(int column = 0; column < this->getWidth(); ++column)
            {
				if (this->data[row][column] == 0)
				{
					cout << "-";
				}

				else
				{
					cout << this->data[row][column];
				}
			}
			cout << endl;
	}
}

void Board::draw()
{
	Sprite *arrow = &m_Arrow;
	Sprite *buffer = &m_BoardTiles.find('0')->second;
	int offsetX = (m_Gc.get_width() - this->getWidth() * buffer->get_width())/2;
	int offsetY = (m_Gc.get_height() - (this->getHeight()-1) * buffer->get_height())/2 ;

	for(int column = 0; column < width; ++column)
	{
		arrow = &m_Arrow;
		if (m_MousePos->x >= column * buffer->get_width() + offsetX + (buffer->get_width() - arrow->get_width())/2 &&
			m_MousePos->x <= column * buffer->get_width() + offsetX + (buffer->get_width() - arrow->get_width())/2 + arrow->get_width() &&
			m_MousePos->y >= offsetY - arrow->get_height() && m_MousePos->y <= offsetY && *m_HasWon == false)
		{
			arrow = &m_ArrowOver;
			if(*m_Clicked && *m_Player%2 == 0)
			{
				*m_HasWon = this->makeMove(column);
				*m_Clicked = false;
			}
		}
		arrow->update(10);
		arrow->draw(m_Gc, column * buffer->get_width() + offsetX + (buffer->get_width() - arrow->get_width())/2, offsetY - arrow->get_height());
	}

	for(int row = 0; row < height; ++row)
    {
		for(int column = 0; column < width; ++column)
        {
			//buffer->set_scale(scaleX, scaleY);
			buffer->update(10);
			buffer->draw(m_Gc, column * buffer->get_width() + offsetX, ((this->getHeight()-1)*buffer->get_height())-(row * buffer->get_height()) + offsetY);

			if (this->data[row][column] != 0)
			{
				char xo = this->data[row][column];
				Sprite *stone = &m_BoardTiles.find(xo)->second;

				//stone->set_scale(scaleX, scaleY);
				stone->update(10);
				stone->draw(m_Gc, column * buffer->get_width() + offsetX, ((this->getHeight()-1)*buffer->get_height())-(row * buffer->get_height()) + offsetY);
			}
        }
    }

	if (*m_HasWon && *m_GameState <= 2)
	{
		*m_GameState += 1;
	}
}

bool Board::hasWon(Field lastStone)
{
	int connected = 1;
	int leftBorder = -1;
	int rightBorder = this->getWidth();
	int lowerBorder = -1;
	int upperBorder = this->getHeight();
	char playerChar = this->data[lastStone.row][lastStone.column];

	// horizontal

	// test left
	for(int i = lastStone.column-1; i > max(leftBorder, (lastStone.column)-4); i--)
	{
		if(this->data[lastStone.row][i] == playerChar)
		{
			++connected;
		}
		
		else
		{
			break;
		}
	}

	// test right
	//cout << lastStone.row << ", " << lastStone.column+1 << " - " << rightBorder << endl;
	for(int i = lastStone.column+1; i < min(rightBorder, (lastStone.column)+4); i++)
	{
		if(this->data[lastStone.row][i] == playerChar)
		{
			++connected;
		}
		
		else
		{
			break;
		}
	}

	// eval horizontal
	if (connected >= 4)
	{
		return true;
	}
	connected = 1;

	// vertical

	// test down
	for(int i = lastStone.row-1; i > max(lowerBorder, (lastStone.row)-4); i--)
	{
		if(this->data[i][lastStone.column] == playerChar)
		{
			++connected;
		}
		
		else
		{
			break;
		}
	}

	// test up
	for(int i = lastStone.row+1; i < min(upperBorder, (lastStone.row)+4); i++)
	{
		if(this->data[i][lastStone.column] == playerChar)
		{
			++connected;
		}
		
		else
		{
			break;
		}
	}

	// eval vertical
	if (connected >= 4)
	{
		return true;
	}
	connected = 1;

	// diagonal1

	// test lowerleft
	for(int i = lastStone.column-1, j = lastStone.row-1; i > max(leftBorder, (lastStone.column)-4) && j > max(lowerBorder, (lastStone.row)-4); i--, j--)
	{
		if(this->data[j][i] == playerChar)
		{
			++connected;
		}
		
		else
		{
			break;
		}
	}

	// test upperright
	for(int i = lastStone.column+1, j = lastStone.row+1; i < min(rightBorder, (lastStone.column)+4) && j < min(upperBorder, (lastStone.row)+4); i++, j++)
	{
		if(this->data[j][i] == playerChar)
		{
			++connected;
		}
		
		else
		{
			break;
		}
	}

	// eval diagonal1
	if (connected >= 4)
	{
		return true;
	}
	connected = 1;

	// diagonal2

	// test upperleft
	for(int i = lastStone.column-1, j = lastStone.row+1; i > max(leftBorder, (lastStone.column)-4) && j < min(upperBorder, (lastStone.row)+4); i--, j++)
	{
		if(this->data[j][i] == playerChar)
		{
			++connected;
		}
		
		else
		{
			break;
		}
	}

	// test lowerright
	for(int i = lastStone.column+1, j = lastStone.row-1; i < min(rightBorder, (lastStone.column)+4) && j > max(lowerBorder, (lastStone.row)-4); i++, j--)
	{
		if(this->data[j][i] == playerChar)
		{
			++connected;
		}
		
		else
		{
			break;
		}
	}

	// eval diagonal2
	if (connected >= 4)
	{
		return true;
	}

	return false;
}

Board::~Board()
{
}

bool Board::makeMove(int column) 
{
	vector<Field> moves = getPossibleMoves();
	for(int i=0; i < moves.size(); i++) 
	{
		//if(moves[i].column == atoi(&m_Input)-1)
		if(moves[i].column == column)
		{
			this->data[moves[i].row][moves[i].column] = 'o';
			*m_Player += 1;
			return this->hasWon(moves[i]);
		}
	}
	return false;
}

vector<Field> Board::getPossibleMoves()
{
	vector<Field> moves;
	for(int w = 0; w < this->getWidth(); w++)
	{
		for(int h = 0; h < this->getHeight(); h++)
		{
			if (this->data[h][w] == 0)
			{
				Field move;
				move.row = h;
				move.column = w;
				moves.push_back(move);
				break;
			}
		}
	}
	return moves;
}