#pragma once
#include "playerselection.h"

PlayerSelection::PlayerSelection(Canvas gc, ResourceManager resources, int* gameState, Point* mousePos, bool* clicked, int* player)
{
	m_Gc = gc;
	m_Resources = resources;
	m_GameState = gameState;
	m_MousePos = mousePos;
	m_Font = Font::Font(m_Gc,"arial", 32);
	m_Button = Sprite::resource(m_Gc, "button", m_Resources);
	m_ButtonOver = Sprite::resource(m_Gc, "buttonover", m_Resources);
	m_Clicked = clicked;
	m_Player = player;

}

PlayerSelection::~PlayerSelection()
{
}

void PlayerSelection::draw()
{
	int gc_width = m_Gc.get_width();
	int gc_height = m_Gc.get_height();
	float scaleX = 1.5f;
	float scaleY = 1.0f;
	std::string text = "Who shall drop the first stone?";
	std::string button1Str = "Human";
	std::string button2Str = "KI";

    Size font1Size = m_Font.get_text_size(m_Gc, button1Str);
	Size font2Size = m_Font.get_text_size(m_Gc, button2Str);
	Size textSize = m_Font.get_text_size(m_Gc, text);

	m_Font.draw_text(m_Gc, gc_width/2 - textSize.width/2, (gc_height/3) - textSize.height, text, Colorf(1.0f, 1.0f, 0.0f, 1.0f));

	Sprite* button = &m_Button;
	if (m_MousePos->x >= gc_width/3 - ((button->get_width()*scaleX)/2) && m_MousePos->x <= gc_width/3 + ((button->get_width()*scaleX)/2))
	{
		if (m_MousePos->y >= ((gc_height/3)*2 - ((button->get_height()*scaleY)/2)) && m_MousePos->y <= ((gc_height/3)*2 + ((button->get_height()*scaleY)/2)))
		{
			button = &m_ButtonOver;
			if (*m_Clicked)
			{
				*m_GameState += 1;
				*m_Clicked = false;
				*m_Player = 0;
			}
		}
	}

	button->set_scale(scaleX, scaleY);
	button->update(10);
	button->draw(m_Gc, (gc_width/3 - ((button->get_width()*scaleX)/2)), ((gc_height/3)*2 - ((button->get_height()*scaleY)/2)));
	m_Font.draw_text(m_Gc, gc_width/3 - font1Size.width/2, (gc_height/3)*2 + font1Size.height/3, button1Str, Colorf(0.0f, 0.0f, 0.0f, 1.0f));

	button = &m_Button;
	if (m_MousePos->x >= (gc_width/3)*2 - ((button->get_width()*scaleX)/2) && m_MousePos->x <= (gc_width/3)*2 + ((button->get_width()*scaleX)/2))
	{
		if (m_MousePos->y >= ((gc_height/3)*2 - ((button->get_height()*scaleY)/2)) && m_MousePos->y <= ((gc_height/3)*2 + ((button->get_height()*scaleY)/2)))
		{
			button = &m_ButtonOver;
			if (*m_Clicked)
			{
				*m_GameState += 1;
				*m_Clicked = false;
				*m_Player = 1;
			}
		}
	}

	button->set_scale(scaleX, scaleY);
	button->update(10);
	button->draw(m_Gc, ((gc_width/3)*2 - ((button->get_width()*scaleX)/2)), ((gc_height/3)*2 - ((button->get_height()*scaleY)/2)));
	m_Font.draw_text(m_Gc, (gc_width/3)*2 - font2Size.width/2, (gc_height/3)*2 + font2Size.height/3, button2Str, Colorf(0.0f, 0.0f, 0.0f, 1.0f));
}